export { default as Homepage } from './components/layout/Header';
export { default as Footer } from './components/layout/Footer';
export { default as Navigation } from './components/layout/Navigation';
export { default as Logo } from './components/layout/Logo';
export { default as Content } from './components/layout/Content';