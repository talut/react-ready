import React from 'react';
import { Row, Col } from 'antd';

const Content = props => (
  <Row style={{ marginTop: 15 }} type="flex" justify="center">
    <Col xs={24} lg={props.colSize}>
      {props.content}
    </Col>
  </Row>
);
export default Content;
