import React from 'react';
import { Col } from 'antd';
import { Link } from 'react-router-dom';

const Logo = () => (
  <Col xs={24} lg={6}>
    <div className="logo">
      <Link to="/">
        <h1>Logo</h1>
      </Link>
    </div>
  </Col>
);
export default Logo;
