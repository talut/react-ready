import React from 'react';

import { Badge, Icon } from 'antd';
import { Link } from 'react-router-dom';

const Navigation = () => (
  <nav>
    <li>
      <Link to={'/'}>
        Link
      </Link>
    </li>
  </nav>
);
export default Navigation;
