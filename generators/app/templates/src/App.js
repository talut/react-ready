import React from 'react';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Homepage } from './screen/layouts';

import './styles/css/main.css';

class App extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <Route exact path="/" component={Homepage}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
