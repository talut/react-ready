var Generator  = require('yeoman-generator');
module.exports = class extends Generator {
  install() { 
    this.spawnCommand('npm', ['install']);
  }

  prompting() {
    const addSassFunc   = (addSass) => {
      if (addSass) {
        return "\"sass\": \"sass --watch .\/src\/styles\/scss:.\/src\/styles\/css --style compressed\"";
      }
      return null;
    };
    const addEslintFunc = (addEslint) => {
      if ((addEslint)) {
        return (',\n"devDependencies": {\n' +
        '"eslint": "^3.19.0",\n' +
        '"eslint-config-airbnb": "^15.0.2",\n' +
        '"eslint-plugin-import": "^2.6.1",\n' +
        '"eslint-plugin-jsx-a11y": "^5.1.1",\n' +
        '"eslint-plugin-react": "^7.1.0"\n}');
      }
      return null;
    };
    var screen          = [
      'src/screen/layouts/Homepage.js',
      'src/screen/layouts.js'
    ];

    var components = [
      'src/screen/components/layout/Header.js',
      'src/screen/components/layout/Footer.js',
      'src/screen/components/layout/Content.js',
      'src/screen/components/layout/Logo.js',
      'src/screen/components/layout/Navigation.js',
      'src/screen/components.js'
    ];

    var publicDirectory = [
      "public/favicon.ico",
      "public/index.html",
      "public/manifest.json",
    ];
    var otherJsFiles    = [
      "src/App.js",
      "src/App.test.js",
      "src/index.css",
      "src/index.js",
      "src/registerServiceWorker.js"
    ];

    return this.prompt([
      {
        type   : 'input',
        name   : 'projectName',
        message: 'Projenizin adı nedir?',
        default: this.appname
      },
      {
        type   : 'input',
        name   : 'projectVersion',
        message: 'Projenizin versiyon numarası nedir?',
        default: "1.0.0"
      },
      {
        type   : 'confirm',
        name   : 'addScreens',
        message: 'Hazır ana ekran eklemek ister misiniz?',
        default: true
      },
      {
        type   : 'confirm',
        name   : 'addComponents',
        message: 'Hazır bileşenleri eklemek ister misiniz?',
        default: true
      },
      {
        type   : 'confirm',
        name   : 'addSass',
        message: 'Sass eklemek ister misiniz?',
        default: true
      },
      {
        type   : 'confirm',
        name   : 'addEslint',
        message: 'Eslint eklemek ister misiniz?',
        default: true
      },
    ]).then((answers) => {
      this.fs.copy(
        this.templatePath("src/styles/css/main.css"),
        this.destinationPath("src/styles/css/main.css")
      );
      if (answers.addSass) {
        this.fs.copy(
          this.templatePath("src/styles/scss/main.scss"),
          this.destinationPath("src/styles/scss/main.scss")
        );
      }
      if (answers.addScreens) {
        for (var value of screen) {
          this.fs.copy(
            this.templatePath(value),
            this.destinationPath(value)
          );
        }
      }
      if (answers.addComponents) {
        for (var value of components) {
          this.fs.copy(
            this.templatePath(value),
            this.destinationPath(value)
          );
        }
      }
      if (answers.addEslint) {
        this.fs.copy(
          this.templatePath('.eslintrc.json'),
          this.destinationPath('.eslintrc.json')
        );

      }
      for (var value of publicDirectory) {
        this.fs.copy(
          this.templatePath(value),
          this.destinationPath(value)
        );
      }
      for (var value of otherJsFiles) {
        this.fs.copy(
          this.templatePath(value),
          this.destinationPath(value)
        );
      }


      this.fs.copyTpl(
        this.templatePath('package.json'),
        this.destinationPath('package.json'),
        {
          appname       : answers.projectName,
          projectVersion: answers.projectVersion,
          sass          : addSassFunc(answers.addSass),
          eslint        : addEslintFunc(answers.addEslint)
        }
      )
      ;
    });
  }

};
